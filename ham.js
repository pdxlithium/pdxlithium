function menu() {
  document.getElementById('bodywrap').style.cssText = 'right:200px;';
  document.getElementById('navwrap').style.cssText = 'right:0px;';
  document.getElementById('reset').style.cssText = 'display:block;opacity:0.8;right:200px;';
  document.getElementById('ham').style.cssText = 'display:none;';
  document.getElementById('menu').style.cssText = 'display:flex;';
}

function reset() {
  document.getElementById('bodywrap').style.cssText = 'right:0px;';
  document.getElementById('navwrap').style.cssText = 'right:-200px;display:none;';
  document.getElementById('reset').style.cssText = 'display:none;opacity:0;right:100vw;';
  document.getElementById('ham').style.cssText = 'display:block;';
  document.getElementById('menu').style.cssText = 'display:none;';


}
function show() {
  document.getElementById('read').style.cssText = 'height:500px;overflow:hidden;transition:height 500ms ease;';
  document.getElementById('show').style.cssText = 'display:none;';
}

function hide() {
  document.getElementById('read').style.cssText = 'height:0px;overflow:hidden;transition:height 500ms ease;';
  document.getElementById('show').style.cssText = 'display:block;';
}
